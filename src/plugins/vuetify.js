// src/plugins/vuetify.js
import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import { i18n } from '@/plugins/i18n';

Vue.use(Vuetify);

export default new Vuetify({
  lang: {
    t: (key, ...params) => i18n.t(key, params),
  },
});
