import Vue from 'vue';
import VueI18n from 'vue-i18n';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);
Vue.use(VueI18n);

const loadMessages = () => {
  let messages = {};
  const locales = require.context('@/locales', true, /[A-Za-z0-9-_,\s]+\.json$/i);

  locales.keys().forEach(key => {
    const matched = key.match(/([A-Za-z0-9-_]+)\./i);

    if (matched && matched.length > 1) {
      let ctx;
      const locale = key.split('/')[1];

      messages[locale] = { ...messages[locale], ...locales(key) };

      // Needed for Validations !!
      messages[locale]['fields'] = { ...messages[locale]['fields'], ...locales(key) };

      // Importing Validation Messages
      ctx = require(`vee-validate/dist/locale/${locale}.json`);
      messages[locale]['validation'] = ctx.messages;

      // Importing Vuetify Messages
      ctx = require(`vuetify/es5/locale/${locale}.js`);
      messages[locale]['$vuetify'] = ctx.default;
    }
  });
  return messages;
};

export const i18n = new VueI18n({
  locale: process.env.VUE_APP_I18N_LOCALE || 'en',
  fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || 'en',
  messages: loadMessages(),
});
