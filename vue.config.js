module.exports = {
  pluginOptions: {
    i18n: {
      locale: "en",
      fallbackLocale: "en",
      localeDir: "locales",
      enableInSFC: true,
    },
  },
  devServer: {
    // open: process.platform === 'darwin',
    open: true,
    compress: true,
    host: "0.0.0.0",
    port: 9069, // CHANGE YOUR PORT HERE!
    https: false,
    hotOnly: false,
  },
};
