# Hi Luke,

## First of all...

a big THANK YOU for willing to help me out...
(and sorry for my poor English & spelling mistakes... it ain't my native language...)

## Description of the problem.

It would be great if vee-validate can be used in combination with Quasar.  
As you will see in this demo application, it works fine with Vue2, in combination with Vuetify.  
I'm an (ex-) Vuetify user & lover... :)

## The goal (i.e. dream scenario)

Rebuilding this APP with Vue v3, Quasar v2, Vee-validate (v3 or, even better, the new v4)

## Why Vee-Validate ?

- It has A LOT of "built-in" error checks.
- It's Multi-language... It supports a TON of languages (all error messages are pre-defined per language, even with parameters)
- It's very easy to implement (at least... in Vuetify :)

## Versions in this DEMO App

This Demo APP uses

- Vue v2, (just because Vuetify isn't ready yet for Vue3 at this moment)
- Vee-validate v3, though, v4 is on it's way...

## About this application

Well, I tried to keep it as easy as possible :)

I've 2 "entities" in my application, named "Players" and "Teams". (no further importance)

In a real world app, you can consider this as 2 tables with fields, coming from a database...
And as you can guess... a Team can have many Players...

### How to test...

Just play with the language selector on top of the page, and see what's happening :)

## The code.

I've tried to document to code as clear as possible.
It's all basic JS and basic Vuetify. (Vuetify syntax is very similar to Quasar)

As you can see in the **locales** folder, I like to keep my translations "separated" per entity.  
Just to avoid 'large' translation files when having a lot of entities...  
The **\_app.json** file contains some "application-wide" translations, (in other words, they are NOT entity-related)

The translation files are loaded in a 'dynamic' way...
See **src/plugins/i18n.js** file for details.
It would be great if we can keep it this way. (in fact, it's a similar 'system' as you are loading your routes in your "Quasar Component Description Series" on Youtube)

I think (but I might be wrong), we've to bring some functionality (defined in the plugins folder) to the 'boot' folder of Quasar.

## So, that's it...

Luke, once again, thanks a lot for willing to help me out !

Kind regards,  
Johnny (Belgium)
